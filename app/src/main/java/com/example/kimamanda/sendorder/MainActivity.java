package com.example.kimamanda.sendorder;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import java.text.DecimalFormat;


import org.w3c.dom.Text;

import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    EditText campoNome;
    TextView resumo, numero;
    String nome;
    CheckBox opcaoChantilly;
    CheckBox opcaoChocolate;
    Button botao;

    String strNumero = "1", cobertura;
    int aux = 0, numeroCafes = 1, valorCafe = 3, valorTotalCafe, valorCobertura, totalCobertura, totalValorReal;
    double valorDolar = 3.25, totalDolar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        campoNome = (EditText) findViewById(R.id.campoNome);
        opcaoChantilly = (CheckBox) findViewById(R.id.opcaoChantilly);
        opcaoChocolate = (CheckBox) findViewById(R.id.opcaoChocolate);
        numero = (TextView) findViewById(R.id.numero);
        resumo = (TextView) findViewById(R.id.resumo);

    }

    public void check(View v) {

        if (opcaoChantilly.isChecked()) {

            if (opcaoChocolate.isChecked()) {   //chantilly e chocolate
                aux = 3;
                valorCobertura = 3;
                cobertura = opcaoChantilly.getText().toString() + ", " + opcaoChocolate.getText().toString();
            } else {    //chantilly
                aux = 2;
                valorCobertura = 2;
                cobertura = opcaoChantilly.getText().toString();
            }
        } else {
            if (opcaoChocolate.isChecked()) { //chocolate
                aux = 1;
                valorCobertura = 1;
                cobertura = opcaoChocolate.getText().toString();
            } else {    //nenhum
                aux = 0;
                valorCobertura = 0;
                cobertura = getResources().getString(R.string.semCobertura);;
            }
        }
    }


    public void soma(View v) {

        numeroCafes = Integer.parseInt(numero.getText().toString());

        if (numeroCafes > 99) {
            Context context = getApplicationContext();
            int duration = Toast.LENGTH_LONG;
            CharSequence text = getResources().getString(R.string.pedidoMaximo);
            Toast toast = Toast.makeText(context, text, duration);
            toast.show();
        } else {
            numeroCafes++;
            converteNumero(v, numeroCafes);
        }
    }

    public void subtrai(View v) {
        numeroCafes = Integer.parseInt(numero.getText().toString());


        if (numeroCafes < 2) {
            Context context = getApplicationContext();
            int duration = Toast.LENGTH_LONG;
            CharSequence text = getResources().getString(R.string.pedidoMinimo);
            Toast toast = Toast.makeText(context, text, duration);
            toast.show();
        } else {
            numeroCafes--;
            converteNumero(v, numeroCafes);
        }
    }

    public void converteNumero(View v, double i) {
        strNumero = Double.toString(i);
        numero.setText((strNumero).replace(".0", ""));
    }

    public void compartilha (View v, String descricao, String valor, String assunto){
        Intent myIntent = new Intent(Intent.ACTION_SEND);
        myIntent.setType("text/plain");
        String shareSub = assunto;
        String[] email = new String[]{"kim.kohlenbach@digitaldesk.com.br"};
        myIntent.putExtra(Intent.EXTRA_SUBJECT,shareSub);
        String shareBody = getResources().getString(R.string.campo_nome) + ": " + nome + "\n" + getResources().getString(R.string.campo_cobertura) + ": " + cobertura + "\n" + getResources().getString(R.string.campo_quantidade) + ": " + String.valueOf(strNumero).replace(".0", "") + "\n" + getResources().getString(R.string.campo_total) + valor.replace(".",",");
        myIntent.putExtra(Intent.EXTRA_TEXT,shareBody);
        myIntent.putExtra(Intent.EXTRA_EMAIL, email);
        startActivity(Intent.createChooser(myIntent, descricao));
    }

    public void mostraResumo(View v) {
        check(v);
        valorTotalCafe = numeroCafes * valorCafe;
        totalCobertura = numeroCafes * valorCobertura;
        totalValorReal = valorTotalCafe + totalCobertura;

        totalDolar = (double) totalValorReal / valorDolar;

        DecimalFormat df = new DecimalFormat("0.##");
        String totalValorDolar = df.format(totalDolar);

        Locale locale = Locale.getDefault();

        if (locale.getLanguage() == "en") {
            compartilha(v, getResources().getString(R.string.metodoCompartilha), totalValorDolar, getResources().getString(R.string.assuntoEmail));
            //resumo.setText(getResources().getString(R.string.campo_nome) +": " + nome + "\n" + getResources().getString(R.string.campo_cobertura) + ": " + cobertura + "\n" + getResources().getString(R.string.campo_quantidade) + ": " + String.valueOf(strNumero).replace(".0", "") + "\n" + getResources().getString(R.string.campo_total) + totalValorDolar.replace(".",","));
        } else {
            compartilha(v, getResources().getString(R.string.metodoCompartilha), String.valueOf(totalValorReal), getResources().getString(R.string.assuntoEmail));
        }
    }

    public void verificaNome(View v) {
        Context context = getApplicationContext();
        int duration = Toast.LENGTH_LONG;
        CharSequence text = getResources().getString(R.string.inserirNome);
        Toast toast = Toast.makeText(context, text, duration);
        toast.show();
    }

    public void confirmar(View v) {

        nome = campoNome.getText().toString();

        if ("".equals(nome)) {
            verificaNome(v);
        } else {
            mostraResumo(v);

        }
    }
}



